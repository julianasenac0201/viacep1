﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;


namespace ViaCep.Views
{
    public class Endereco
    {

        public string Cep { get; set; }
        public string Logradouro { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }
        public string Localidade { get; set; }
        public string UF { get; set; }
        public string Unidade { get; set; }
        public int IBGE { get; set; }
        public int Gia { get; set; }

    }
        public async void BuscarEndereco()

    {
        Console.WriteLine("Iniciando consumo da API");
        Uri url = new Uri("https://viacep.com.br/ws/01001000/");

        using HttpResponseMessage httpResponse = await Services.HttpService.GetRequest(url.AbsoluteUri);

        if (httpResponse.IsSuccessStatusCode)
        {
            string stringResponse = httpResponse.Content.ReadAsStringAsync().Result;
            Console.WriteLine("\n=====");
            Console.WriteLine(stringResponse);
        }

    } 

}
